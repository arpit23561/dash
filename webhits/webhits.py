import dash, os, re, urllib3, time, json, fileinput, plotly
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import datetime as dt
import plotly.plotly as py

app = dash.Dash()
app.scripts.config.serve_locally = True
http = urllib3.PoolManager()
now = dt.datetime.now()
pwd = os.getcwd()
now_date = now.strftime("%Y-%m-%d")
servers = {
#    '2admin1':'admin',
#    '2adminkafka1':'admin',
#   '2asset1':'assets',
#    '2asset2':'assets',
#    '2bl1':'bl',
#    '2bl2':'bl',
#    '2group1':'group',
#    '2group2':'group',
#    '2school1':'school',
#    '2school2':'school',
#    '2ums1':'ums',
#    '2ums2':'ums',
    '2webui1':'fliplearn'
#    '2webui2':'fliplearn'
#    'content1':'content',
#    'logs2':'logs',
#    'marketplace3':'marketplace',
#    'marketplace4':'marketplace',
#    'marketplacejob':'marketplace'
}

def make():
    for component in servers:
        page = ''
        while page == '':
            try:
                response = http.request('GET', 'http://serverlogs.fliplearn.com/prod_logs/'+now_date+'/'+component+'/'+servers[component]+'.access.log')
                break
            except:
                print("Connection refused by the server..")
                time.sleep(2)
                continue
        data = response.data.decode('utf-8')
        with open(pwd+"/files/"+component+"_out_tmp.csv", "w") as out_file:
            with open(pwd+"/files/"+component+"_in.txt", "w") as in_file:
                in_file.write(data)
                in_file.close()
                with open(pwd+"/files/"+component+"_in.txt", "r") as in_file:
                    for line in in_file:
                        regex = re.findall( r'(?:\d{1,3})\.(?:\d{1,3})\.(?:\d{1,3})\.(?:\d{1,3})', line )
                        out_file.writelines(str(regex[0]+"\n"))

def dedupicate():
    for component in servers:
        seen = set() # set for fast O(1) amortized lookup
        for line in fileinput.FileInput(pwd+"/files/"+component+"_out_tmp.csv", inplace=1):
            line = line.rstrip()
            if line in seen: continue # skip duplicate
            seen.add(line)
            print (line)
        
def makecsv():
    for component in servers:
        with open(pwd+"/files/"+component+"_out_tmp.csv", "r") as in_file:
            with open(pwd+"/files/"+component+"_out.csv", "w") as csv_file:
                csv_file.write("ip,latitude,longitude,city"+"\n")
                csv_file.close()
                for ip in in_file:
                    ip = ip.rstrip()
                    location_obj = http.request('GET', 'http://geoip.nekudo.com/api/'+ip)
                    location = json.loads(location_obj.data.decode('utf8'))
                    #location = location.get("city")
                    latitude = str(location.get("location",{}).get("latitude"))
                    longitude = str(location.get("location",{}).get("longitude"))
                    city = str(location.get("city"))
                    with open(pwd+"/files/"+component+"_out.csv", "a") as csv_file:
                       data = str(ip+','+latitude+','+longitude+','+city)
                       csv_file.write(data+"\n")

def app_plot():
    for component in servers:
        df = pd.read_csv(pwd+"/files/"+component+"_out.csv")
        df_similar1 = pd.concat(g for _, g in df.groupby("city") if len(g) > 1)
        df_similar2 = list(df_similar1.city)
        df_similar3 = {x:df_similar2.count(x) for x in df_similar2}
        df_city = list((df_similar3.keys()))
        df_city_num = list((df_similar3.values()))   
        trace1 = {
            "hoverinfo": "text",
            "lat": list(df.latitude),
            "lon": list(df.longitude), 
            "marker": {
                "opacity": 0.5, 
                "reversescale": False, 
                "showscale": False, 
                "size": 8, 
                "sizemode": "area", 
                "symbol": "circle"
            }, 
            "mode": "markers", 
            "name": "lon", 
            "text": list(df.city), 
            "type": "scattergeo",
            "visible": True
        }
        app.layout = html.Div([
            html.H1('User hits', style={'color':'blue', 'fontsize': 2, 'marginTop': 5 ,'allign': 'center'}),
            dcc.Graph(
                id='map',
                figure= {
                    'data': [trace1],
                    'layout': {
                        'geo': {
                            'center': {
                                'lat': 4.78630639767, 
                                'lon': 41.7108738088
                            },
                            "countrycolor": "rgb(126, 126, 126)",
                            "landcolor": "rgb(0, 84, 0)",
                            "lonaxis": {"showgrid": False}, 
                            "projection": {
                                "rotation": {
                                    "lat": 0.43337000599, 
                                    "lon": 53.0116178006
                                },
                                "scale": 0.80190511588, 
                                "type": "orthographic",
                                #"type": "eckert4"
                            }, 
                            "scope": "world",
                            "showcoastlines": True, 
                            "showcountries": True, 
                            "showland": True
                        }, 
                        "hovermode": "closest", 
                        "legend": {
                            "font": {"family": "Raleway"}, 
                            "orientation": "v"
                        }, 
                        "margin": {
                            "r": 0, 
                            "t": 0, 
                            "b": 0, 
                            "l": 0, 
                            "pad": 0
                        }
                        #"paper_bgcolor": "rgb(255, 255, 255)", 
                        #"title": "<br>"
                    }
                }),
                dcc.Graph(
                id='city_data',
                figure={
                    'data': [
                        {'x':df_city , 'y':df_city_num , 'type': 'bar'},
                    ],
                    'layout': {
                       # 'title': 'Dash Data Visualization'
                    }       
                }
            )
    ])
    app.css.append_css({
        'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'
    })   
    if __name__ == '__main__':
        app.run_server(debug=True)     

#make()
#dedupicate()
#makecsv() 
app_plot()
        