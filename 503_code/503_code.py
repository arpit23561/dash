import dash, os
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd

app = dash.Dash()
app.scripts.config.serve_locally = True
http = urllib3.PoolManager()
now = dt.datetime.now()
pwd = os.getcwd()
now_date = now.strftime("%Y-%m-%d")
servers = {
#    '2admin1':'admin',
#    '2adminkafka1':'admin',
#   '2asset1':'assets',
#    '2asset2':'assets',
    '2bl1':'bl',
#    '2bl2':'bl',
#    '2group1':'group',
#    '2group2':'group',
#    '2school1':'school',
#    '2school2':'school',
#    '2ums1':'ums',
#    '2ums2':'ums',
#    '2webui1':'fliplearn'
#    '2webui2':'fliplearn'
#    'content1':'content',
#    'logs2':'logs',
#    'marketplace3':'marketplace',
#    'marketplace4':'marketplace',
#    'marketplacejob':'marketplace'
}

def make():
    for component in servers:
        page = ''
        while page == '':
            try:
                response = http.request('GET', 'http://serverlogs.fliplearn.com/prod_logs/'+now_date+'/'+component+'/'+servers[component]+'.access.log')
                break
            except:
                print("Connection refused by the server..")
                time.sleep(2)
                continue
        data = response.data.decode('utf-8')
        with open(pwd+"/files/"+component+"_out_tmp.csv", "w") as out_file:
            with open(pwd+"/files/"+component+"_in.txt", "w") as in_file:
                in_file.write(data)
                in_file.close()
                with open(pwd+"/files/"+component+"_in.txt", "r") as in_file:
                    for line in in_file:
                        regex = re.findall( r'(?:\d{1,3})\.(?:\d{1,3})\.(?:\d{1,3})\.(?:\d{1,3})', line )
                        out_file.writelines(str(regex[0]+"\n"))